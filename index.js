const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 4000

dotenv.config()


mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.uwt1xhh.mongodb.net/S35-Activity?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
)

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))


const userSchema = new mongoose.Schema({
	username : String,
	password : String
	}
)

const User = mongoose.model('User', userSchema)

app.use(express.json())
app.use(express.urlencoded({extended : true}))

app.post('/signup', (request, response) => {
	User.findOne({name: request.body.username}, (error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send('The User already exists!')
		} else {
			let newUser = new User({
				username : request.body.username,
				password : request.body.password
			})

			newUser.save((error, savedTask) => {
				if(error){
					return console.error(error)
				}

				return response.status(201).send('New User registered!')
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}`))